const express = require('express');
var admin = require('firebase-admin');
var serviceAccount = require("/var/www/dtris_lobby_test/puzzle-battle-31925608-firebase-adminsdk-1m0e9-8e028dec3a.json");
var bodyParser = require('body-parser');
var request = require('request');
const app = express();

app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json());

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});


app.post('/auth',(req, res) => {
    var idToken = req.body.token;
    var username = req.body.name;
    admin.auth().verifyIdToken(idToken)
  .then(function(decodedToken) {
    //let uid = decodedToken.uid;
    var userdata = {
    UserID: decodedToken.uid,
    name: username
    }
    var req = requestUserData(JSON.stringify(userdata), function(result){
      res.send(result);
    });
   // res.send(uid);
    // ..
  }).catch(function(error) {
    // Handle error
  });
});

function requestUserData(data, callback){
  let opts = {
    uri: 'https://acbda158w5.execute-api.ap-northeast-2.amazonaws.com/RequestUserData',
    method: 'POST',
    body: data,
    //json:true,
    headers:{'Content-Type': 'application/json',
             'Content-Length': data.length
              }
   }

    request(opts, function(error, response){
      callback(response.body);
    });

}
app.get('/', (req, res) => {
  res.send('AWS exercise의 A project입니다.');
});

app.listen(3000, () => {
  //console.log('Example app listening on port 3000!');
});

app.get('/health', (req, res) => {
  res.status(200).send();
});
